﻿using Newtonsoft.Json;
using Task12_SkjelinOttosen.Models;

namespace Task12_SkjelinOttosen.JsonHelper
{
    // Helper class for serializing C# objects to Json objects
    public static class JsonSerializer
    {
        // Serialize Student object to Json object
        public static string Serializer(Student student)
        {
            string jsonString = JsonConvert.SerializeObject(student, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return jsonString;
        }

        // Serialize Professor object to Json object
        public static string Serializer(Professor professor)
        {
            string jsonString = JsonConvert.SerializeObject(professor, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return jsonString;
        }
    }
}
