﻿using System;
using Microsoft.EntityFrameworkCore;
using Task12_SkjelinOttosen.CRUD;
using Task12_SkjelinOttosen.Models;
using JsonSerializer = Task12_SkjelinOttosen.JsonHelper.JsonSerializer;
using Task12_SkjelinOttosen.WriteToFile;

namespace Task12_SkjelinOttosen
{
    /* READ ME before running the program */
    // The program displays methods that have already been run against the database 
    // and will therefore give an error message during a new execution
    // The Ids used in the GetStudent and GetProfessor methods are unique 
    // and must be retrieved again form the database when creating new objects
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Entity classes
                Student sarah = new Student()
                {
                    FirstName = "Sarah",
                    LastName = "Phillips Von Bark",
                    DateOfBirth = new DateTime(1999, 1, 12),
                    Address = "Street 1, 3003 Wood Street",
                    PhoneNumber = "99010112"
                };

                Student jenny = new Student()
                {
                    FirstName = "Jenny",
                    LastName = "SpringField",
                    DateOfBirth = new DateTime(1999, 3, 29),
                    Address = "Alley 2, 2002 Ally Road",
                    PhoneNumber = "41101090"
                };

                Student peter = new Student()
                {
                    FirstName = "Petter",
                    LastName = "Boots",
                    DateOfBirth = new DateTime(1999, 5, 28),
                    Address = " Spring Road, 4913 Spring",
                    PhoneNumber = "93993332"
                };

                Professor john = new Professor()
                {
                    FirstName = "John",
                    LastName = "Anderson",
                    DateOfBirth = new DateTime(1980, 4, 13),
                    Address = "Oxford street 23, 3939 Oxford",
                    PhoneNumber = "43113403"
                };

                Professor lill = new Professor()
                {
                    FirstName = "Lill",
                    LastName = "Wang",
                    DateOfBirth = new DateTime(1985, 12, 14),
                    Address = "Lake Road, 9331 Lake",
                    PhoneNumber = "9922110"
                };

                // Adds entity classes to the database
                CRUDHelper.AddProfessor(john);
                CRUDHelper.AddStudent(sarah);

                // Gets entity objects for the database using matching ids
                Student sarahFromDb = CRUDHelper.GetStudent(Guid.Parse("0367d6d7-8028-49a0-95de-08d8485bc7e5"));
                Professor johnFromDb = CRUDHelper.GetProfessor(Guid.Parse("6afc5ab9-9a08-4617-c777-08d8485bc749"));

                // Add a relationship between the student Sarah and the professor John
                CRUDHelper.AddRelation(sarahFromDb, johnFromDb);

                // Serilize professor object to a json file in folder ./Task12_SkjelinOttosen\bin\Debug\netcoreapp3.1\
                FileWriter.WriteToFile(@"professor.json", JsonSerializer.Serializer(johnFromDb));

                // Update methods for the student and professor entity classes
                CRUDHelper.UpdateStudentFirstName(sarahFromDb, "Sarah Louise");
                CRUDHelper.UpdateStudentLastName(sarahFromDb, "Johnsen");
                CRUDHelper.UpdateStudentDateOfBirth(sarahFromDb,new DateTime(2000, 1, 1));
                CRUDHelper.UpdateStudentAddress(sarahFromDb, "New Steet, 3311, New Alley");
                CRUDHelper.UpdateStudentPhoneNumber(sarahFromDb, "44223366");

                CRUDHelper.UpdateProfessorFirstName(johnFromDb, "John Albert");
                CRUDHelper.UpdateProfessorLastName(johnFromDb, "Springfield");
                CRUDHelper.UpdateProfessortDateOfBirth(johnFromDb, new DateTime(1981, 1, 1));
                CRUDHelper.UpdateProfessorAddress(johnFromDb, "Newer Steet, 4411, Newest Alley");
                CRUDHelper.UpdateProfessorPhoneNumber(johnFromDb, "45553363");

                //Delete relation method for the entity classes
                CRUDHelper.DeleteRelation(sarahFromDb, johnFromDb);

                // Delete methods for student and professor
                CRUDHelper.DeleteStudent(sarahFromDb);
                CRUDHelper.DeleteProfessor(johnFromDb);

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
