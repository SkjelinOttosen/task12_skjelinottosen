﻿using System;


namespace Task12_SkjelinOttosen.Models
{
    // Joining class for the Professors and Student entity
    public class ProfessorStudent
    {
        // Part 1 of composite key
        public Guid ProfessorId { get; set; }   
        public Professor Professor { get; set; }

        // Part 2 of composite key 
        public Guid StudentId { get; set; }
        public Student Student { get; set; }
    }
}
