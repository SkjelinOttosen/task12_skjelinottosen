﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task12_SkjelinOttosen.Models
{
    // Entity class for Professor
    // Includes Student collection navigation property 
    public class Professor 
    {
        // Primary key
        [Key]
        public Guid Id { get; set; }
      
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        [MaxLength(95)]
        public string Address { get; set; }

        [MaxLength(15)]
        public string PhoneNumber { get; set; }

        //Navigation propety for Professor 
        //Used  for one-to-one or one-to.many relationship
        //public Guid StudentId { get; set; }
        //public Professor Student{ get; set; }

        // Collection navigation property
        // Used for many-to-many relationship
        public ICollection<ProfessorStudent> ProfessorHasStudents { get; set; }

        public Professor()
        {
            ProfessorHasStudents = new List<ProfessorStudent>();
        }
    }
}
