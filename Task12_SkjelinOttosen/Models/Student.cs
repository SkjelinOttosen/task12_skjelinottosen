﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task12_SkjelinOttosen.Models
{
    // Entity class for Student
    // Includes Professor collection navigation property 
    public class Student
    {
        // Primary key
        [Key]
        public Guid Id { get; set; }
        
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        [MaxLength(95)]
        public string Address { get; set; }

        [MaxLength(15)]
        public string PhoneNumber { get; set; }

        // Navigation propety for Professor
        // Used for one-to-one or one-to-many relationship
        // public Guid ProfessorId { get; set; }
        // public Professor Professor { get; set; }

        // Collection navigation property 
        // Used for many-to-many relationship
        public ICollection<ProfessorStudent> StudentHasProfessors { get; set; }

        public Student()
        {
            StudentHasProfessors = new List<ProfessorStudent>();
        }
    }
}
