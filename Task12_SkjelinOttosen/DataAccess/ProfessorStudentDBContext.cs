﻿using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Task12_SkjelinOttosen.Models;

namespace Task12_SkjelinOttosen.DataAccess
{
    // DbContext for the database
    // Used by Entity Framework Core
    public class ProfessorStudentDbContext : DbContext
    {
        // DbSets for the entity classes
        public DbSet<Student> Student { get; set; }
        public DbSet<Professor> Professor { get; set; }
        public DbSet<ProfessorStudent> ProfessorStudent { get; set; }

        // Connection string for the database.
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
            {
                DataSource = @"PC7383\SQLEXPRESS",
                InitialCatalog = "Tak12",
                IntegratedSecurity = true
            };
            optionsBuilder.UseSqlServer(builder.ConnectionString.ToString());
        }

        // Overrrides the configuration for the creating of the database
        protected override void OnModelCreating(ModelBuilder modelbuilder) 
        {
            // Creates composite key for the joining entity ProfessorStudent
            modelbuilder.Entity<ProfessorStudent>().HasKey(ps => new { ps.ProfessorId, ps.StudentId });
        }
    }
}
