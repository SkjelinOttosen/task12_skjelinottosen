﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Task12_SkjelinOttosen.DataAccess;
using Task12_SkjelinOttosen.Models;

namespace Task12_SkjelinOttosen.CRUD
{
    // Helper class that contains CRUD operations for reading and writing to the database
    // Uses UpdateHelper class 
    public static class CRUDHelper
    {
        // Adds a new student
        public static void AddStudent(Student student)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Add student and saves changes
                    data.Student.Add(student);
                    data.SaveChanges();
                    Console.WriteLine("Saved");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Adds a new professor
        public static void AddProfessor(Professor professor)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Adds porfessor and saves changes
                    data.Professor.Add(professor);
                    data.SaveChanges();
                    Console.WriteLine("Saved");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Adds a relation between a Student and a Professor
        public static void AddRelation(Student student, Professor professor)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {

                    // Stores student and professor ids
                    Guid studentId = student.Id;
                    Guid professorId = professor.Id;

                    // Initalize ProfessorStudent objects
                    ProfessorStudent professorStudentToList = new ProfessorStudent() { StudentId = studentId, Student = student, ProfessorId = professorId, Professor = professor };
                    ProfessorStudent professorStudentToDb = new ProfessorStudent() { StudentId = studentId, ProfessorId = professorId };

                    // Adds the ProfessorStudent object to the relationship lists for the entity classes
                    student.StudentHasProfessors.Add(professorStudentToList);
                    professor.ProfessorHasStudents.Add(professorStudentToList);

                    // Adds the composite key to the database
                    data.ProfessorStudent.Add(professorStudentToDb);
                    data.SaveChanges();
                    Console.WriteLine("Added relationship");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Get student by Id
        // Used a Globally Unique Identifier (Guid)
        public static Student GetStudent(Guid id)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Gets student from the database including relationships
                    Student studentFromDB = data.Student.Where(s => s.Id == id).Include(s => s.StudentHasProfessors).Single();

                    // Returns the student object
                    return studentFromDB;
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        // Get professor by id;
        // Used a Globally Unique Identifier (Guid)
        public static Professor GetProfessor(Guid id)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Gets professor from the database including relationships
                    Professor professorFromDB = data.Professor.Where(p => p.Id == id).Include(p => p.ProfessorHasStudents).Single();

                    // Returns the professor object
                    return professorFromDB;
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        // Gets all students
        public static List<Student> GetAllStudents()
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Gets all students from the database including relationships
                    List<Student> allStudents = data.Student.Include(s => s.StudentHasProfessors).ToList();

                    // Returns all student objects
                    return allStudents;
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        // Gets all professors
        public static List<Professor> GetAllProfessors()
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Gets all professors from the database including relationships
                    List<Professor> allProfessors = data.Professor.Include(p => p.ProfessorHasStudents).ToList();

                    // Returns all professor objects
                    return allProfessors;
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }
        
        /* Update methods for student  and professor uses the UpdateHelper class handling updates of various properties.*/

        // Updates student first name
        public static void UpdateStudentFirstName(Student student, string newFirstName)
        {
            UpdateHelper.Update(student, newFirstName, student.LastName, student.DateOfBirth, student.Address, student.PhoneNumber);
        }

        // Updates student last name
        public static void UpdateStudentLastName(Student student, string newLastName)
        {
            UpdateHelper.Update(student, student.FirstName, newLastName, student.DateOfBirth, student.Address, student.PhoneNumber);
        }

        // Updates student date of birth
        public static void UpdateStudentDateOfBirth(Student student, DateTime newDateOfBirth)
        {
            UpdateHelper.Update(student, student.FirstName, student.LastName, newDateOfBirth, student.Address, student.PhoneNumber);
        }

        // Update student adress
        public static void UpdateStudentAddress(Student student, string newAddress)
        {
            UpdateHelper.Update(student, student.FirstName, student.LastName, student.DateOfBirth, newAddress, student.PhoneNumber);
        }

        // Updates student phone number
        public static void UpdateStudentPhoneNumber(Student student, string newPhoneNumber)
        {
            UpdateHelper.Update(student, student.FirstName, student.LastName, student.DateOfBirth, student.Address, newPhoneNumber);
        }


        // Update professor first name
        public static void UpdateProfessorFirstName(Professor professor, string newFirstName)
        {
            UpdateHelper.Update(professor, newFirstName, professor.LastName, professor.DateOfBirth, professor.Address, professor.PhoneNumber);
        }

        // Updates professor last name
        public static void UpdateProfessorLastName(Professor professor, string newLastName)
        {
            UpdateHelper.Update(professor, professor.FirstName, newLastName, professor.DateOfBirth, professor.Address, professor.PhoneNumber);
        }

        // Updates professor date of birth
        public static void UpdateProfessortDateOfBirth(Professor professor, DateTime newDateOfBirth)
        {
            UpdateHelper.Update(professor, professor.FirstName, professor.LastName, newDateOfBirth, professor.Address, professor.PhoneNumber);
        }

        // Updates professor address
        public static void UpdateProfessorAddress(Professor professor, string newAddress)
        {
            UpdateHelper.Update(professor, professor.FirstName, professor.LastName, professor.DateOfBirth, newAddress, professor.PhoneNumber);
        }

        // Updates professor phone number.
        public static void UpdateProfessorPhoneNumber(Professor professor, string newPhoneNumber)
        {
            UpdateHelper.Update(professor, professor.FirstName, professor.LastName, professor.DateOfBirth, professor.Address, newPhoneNumber);
        }

        // Deletes the relationship between student and professor
        public static void DeleteRelation(Student student, Professor professor)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {

                    // Stores student and professor ids
                    Guid studentId = student.Id;
                    Guid professorId = professor.Id;

                    // Removes the relationship 
                    data.ProfessorStudent.Remove(data.ProfessorStudent.Find(professorId, studentId));

                    // Saves changes
                    data.SaveChanges();
                    Console.WriteLine("Deleted relationship");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Delete student
        public static void DeleteStudent(Student student)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Delete student including all relationships
                    data.Student.Remove(student);

                    // Saves changes
                    data.SaveChanges();
                    Console.WriteLine("Deleted");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Delete professor
        public static void DeleteProfessor(Professor professor)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Delete professor including all relationships
                    data.Professor.Remove(professor);

                    // Saves changes
                    data.SaveChanges();
                    Console.WriteLine("Deleted");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
