﻿using Microsoft.EntityFrameworkCore;
using System;
using Task12_SkjelinOttosen.DataAccess;
using Task12_SkjelinOttosen.Models;

namespace Task12_SkjelinOttosen.CRUD
{
    // Helper class used by CRUDHelper class for handling updates of various properties.
    public static class UpdateHelper
    {
        // Update student
        public static void Update(Student student, string firstName, string lastName, DateTime dateOfBirth, string address, string phoneNumber)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Assigning new values to Student
                    student.FirstName = firstName;
                    student.LastName = lastName;
                    student.DateOfBirth = dateOfBirth;
                    student.Address = address;
                    student.PhoneNumber = phoneNumber;

                    // Updates student and saves the update
                    data.Student.Update(student);
                    data.SaveChanges();
                    Console.WriteLine("Updated");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Updates professor
        public static void Update(Professor professor, string firstName, string lastName, DateTime dateOfBirth, string address, string phoneNumber)
        {
            try
            {
                using (var data = new ProfessorStudentDbContext())
                {
                    // Assigning new values to Professor
                    professor.FirstName = firstName;
                    professor.LastName = lastName;
                    professor.DateOfBirth = dateOfBirth;
                    professor.Address = address;
                    professor.PhoneNumber = phoneNumber;
                   
                    // Updates Professor and saves the update
                    data.Professor.Update(professor);
                    data.SaveChanges();
                    Console.WriteLine("Updated");
                };
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
