﻿using System;
using System.IO;

namespace Task12_SkjelinOttosen.WriteToFile
{
    public static class FileWriter
    {
        // Helper class for writing to file.
        // Uses default location in folder ./Task12_SkjelinOttosen\bin\Debug\netcoreapp3.1\
        public static void WriteToFile(string fileName, string inputText)
        {
            try
            {           
                // Checks if file exists
                if (!File.Exists(fileName))
                {
                    //Creates the file
                    using (FileStream fileStream = File.Create(fileName)) { };
                    string path = Path.GetFullPath(fileName);
                    Console.WriteLine($"Created {fileName} in folder {path}");
                }

                // Initiate a StreamWriter 
                using (StreamWriter fileWriter = new StreamWriter(fileName))
                {
                    // Writes to file 
                    fileWriter.Write(inputText);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
