﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12_SkjelinOttosen.Migrations
{
    public partial class ManyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorStudents_Professors_ProfessorId",
                table: "ProfessorStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorStudents_Students_StudentId",
                table: "ProfessorStudents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Students",
                table: "Students");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorStudents",
                table: "ProfessorStudents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Professors",
                table: "Professors");

            migrationBuilder.RenameTable(
                name: "Students",
                newName: "Student");

            migrationBuilder.RenameTable(
                name: "ProfessorStudents",
                newName: "ProfessorStudent");

            migrationBuilder.RenameTable(
                name: "Professors",
                newName: "Professor");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorStudents_StudentId",
                table: "ProfessorStudent",
                newName: "IX_ProfessorStudent_StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Student",
                table: "Student",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorStudent",
                table: "ProfessorStudent",
                columns: new[] { "ProfessorId", "StudentId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Professor",
                table: "Professor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorStudent_Professor_ProfessorId",
                table: "ProfessorStudent",
                column: "ProfessorId",
                principalTable: "Professor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorStudent_Student_StudentId",
                table: "ProfessorStudent",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorStudent_Professor_ProfessorId",
                table: "ProfessorStudent");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorStudent_Student_StudentId",
                table: "ProfessorStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Student",
                table: "Student");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorStudent",
                table: "ProfessorStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Professor",
                table: "Professor");

            migrationBuilder.RenameTable(
                name: "Student",
                newName: "Students");

            migrationBuilder.RenameTable(
                name: "ProfessorStudent",
                newName: "ProfessorStudents");

            migrationBuilder.RenameTable(
                name: "Professor",
                newName: "Professors");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorStudent_StudentId",
                table: "ProfessorStudents",
                newName: "IX_ProfessorStudents_StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Students",
                table: "Students",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorStudents",
                table: "ProfessorStudents",
                columns: new[] { "ProfessorId", "StudentId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Professors",
                table: "Professors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorStudents_Professors_ProfessorId",
                table: "ProfessorStudents",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorStudents_Students_StudentId",
                table: "ProfessorStudents",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
